#!/usr/bin/env python3
import pathlib
import re

from setuptools.extern.packaging.version import Version


def version():
    """Extract version information from source code"""
    version_path = pathlib.Path(__file__).parent.joinpath("version.h")

    matcher = re.compile('[\t ]*#define[\t ]+OPENQL_VERSION_STRING[\t ]+"(.*)"')
    version = None
    with open(version_path, "r") as f:
        for ln in f:
            m = matcher.match(ln)
            if m:
                version = m.group(1)
                break

    return version


if __name__ == "__main__":
    normalized_version = Version(version())
    print(normalized_version)
