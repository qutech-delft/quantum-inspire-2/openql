# OpenQL

This repository uses gitlab ci to build and push python wheels of the [OpenQL project](https:github.com/qutech-delft/openql) to the gitlab PyPI of this repository. 

## Installation

```bash
pip install qutechopenql -i https://gitlab.com/api/v4/projects/37640328/packages/pypi/simple
```
## Usage

See the [OpenQL documentation](https://openql.readthedocs.io/en/latest/) on readthedocs.

## Release Process

>  **Warning: pypi allows versions to be published only once!** Before you trigger a new push using CI/CD always check that the version in [version.h](version.h) is one that is not alreay uploaded to our [gitlab PyPI](https://gitlab.com/qutech-delft/quantum-inspire-2/openql/-/packages/). Otherwise this pipeline will fail.
>

For creating a release one should first check out the commit in the OpenQL git submodule that you want to build. By doing something along the lines of:

```bash
cd openql
git checkout <branch|commit|tag>
```

After that one should modify the value of `OPENQL_VERSION_STRING` [version.h](version.h) in the root of this gitlab repository to a new version name. Make sure that the string is set to a [semver](https://semver.org/) prerelease version string, in order to avoid clashes with (future) official openql releases.

Finally one should merge these changes in to the master branch and manually trigger the `push_openql` step in the newly created [gitlab pipeline](https://gitlab.com/qutech-sd/quantum-inspire-2/platform/-/pipelines).


## License

The licence can be found in the [LICENSE](LICENSE) file.
